<?php

namespace App\Service;


class calculateFizzBuzz2
{
    /**
     * @param int $first
     * @param int $last
     * @return string
     * Recibimos primer y último número y comprobamos los multiplos de 3 y 5 entre ellos (ambos incluidos)
     */
    public function calculateFizzBuzz2(int $first = 1, int $last = 30): string
    {
        //Creamos array
        do {

            if (fmod($first, 15) == 0) {
                //Multiplo de 3 y 5
                $array_data[] = 'FizzBuzz';
            } elseif (fmod($first, 5) == 0) {
                //Multiplo de 5
                $array_data[] = 'Buzz';
            } elseif (fmod($first, 3) == 0) {
                //Multiplo de 3
                $array_data[] = 'Fizz';
            } else {
                //Resto de valores
                $array_data[] = $first;
            }

            $first++;

        } while ($first <= $last);

        //Convertimos array en string para pintarlo
        $data = implode(', ', $array_data);

        return $data;
    }
}