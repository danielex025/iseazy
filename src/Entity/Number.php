<?php

namespace App\Entity;

use App\Repository\NumberRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NumberRepository::class)
 */
class Number
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    public $first_number;

    /**
     * @ORM\Column(type="integer")
     */
    public $last_number;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datetime;

    /**
     * @ORM\Column(type="text")
     */
    private $fizzbuzz;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstNumber(): ?int
    {
        return $this->first_number;
    }

    public function setFirstNumber(int $first_number): self
    {
        $this->first_number = $first_number;

        return $this;
    }

    public function getLastNumber(): ?int
    {
        return $this->last_number;
    }

    public function setLastNumber(int $last_number): self
    {
        $this->last_number = $last_number;

        return $this;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getFizzbuzz(): ?string
    {
        return $this->fizzbuzz;
    }

    public function setFizzbuzz(string $fizzbuzz): self
    {
        $this->fizzbuzz = $fizzbuzz;

        return $this;
    }

    public function setNumber(Number $number): void
    {
        $this->number = $number;
    }
}
