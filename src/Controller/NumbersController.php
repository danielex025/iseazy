<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\calculateFizzBuzz2;
use App\Entity\Number;
use App\Form\FizzbuzzType;

/**
 * Class NumbersController
 * @package App\Controller
 */
class NumbersController extends AbstractController
{
    /**
     * @Route("/desafio1/fizz/buzz", name="desafio1")
     */
    public function desafio1(): Response
    {
        return $this->render('numbers/desafio1.html.twig');
    }

    /**
     * @Route("/desafio2/fizz/buzz", name="desafio2")
     */
    public function desafio2(calculateFizzBuzz2 $calculateFizzBuzz2, Request $request): Response
    {
        $number = new Number();

        $form = $this->createForm(FizzbuzzType::class, $number, [
            'action' => $this->generateUrl('desafio2'),
            'method' => 'POST'
        ]);

        //Guardamos el formulario en blanco para asegurarnos que no se vuelve a pintar los mismos números
        $form_to_view = $form->createView();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $number->setDatetime(new \DateTime('now'));
            $data = $calculateFizzBuzz2->calculateFizzBuzz2($request->get('fizzbuzz')['first_number'], $request->get('fizzbuzz')['last_number']);
            $number->setFizzbuzz($data);

            $em->persist($number);
            $em->flush();
        } else {
            $data = '';
        }

        return $this->render('numbers/desafio2.html.twig', [
            'form' => $form_to_view,
            'data' => $data,
        ]);
    }
}
