<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class NumbersExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('calculateFizzBuzz', [$this, 'calculateFizzBuzz']),
        ];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('calculateFizzBuzz', [$this, 'calculateFizzBuzz']),
        ];
    }

    /**
     * @param int $first
     * @param int $limit
     * @return string
     * Función para el cálculo de número correlativos con sustitución en números múltiplos de 3 y 5.
     */
    public function calculateFizzBuzz(int $first = 1, int $limit = 30): string
    {
        //Creamos array
        do {

            if (fmod($first, 15) == 0) {
                //Multiplo de 3 y 5
                $array_data[] = 'FizzBuzz';
            } elseif (fmod($first, 5) == 0) {
                //Multiplo de 5
                $array_data[] = 'Buzz';
            } elseif (fmod($first, 3) == 0) {
                //Multiplo de 3
                $array_data[] = 'Fizz';
            } else {
                //Resto de valores
                $array_data[] = $first;
            }

            $limit--;
            $first++;

        } while ($limit > 0);

        //Convertimos array en string para pintarlo
        $data = implode(', ', $array_data);

        return $data;

    }
}
