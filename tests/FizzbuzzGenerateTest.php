<?php

use PHPUnit\Framework\TestCase;
use App\Service\calculateFizzBuzz2;

final class FizzbuzzGenerateTest extends TestCase
{
    public function testPushAndPop(): void
    {
        $data = (new calculateFizzBuzz2)->calculateFizzBuzz2(30, 67);
        $this->assertNotEmpty($data);
        $array_data = explode(', ', $data);
        $this->assertEquals(count($array_data), 38);
        $this->assertEquals($array_data[0], 'FizzBuzz');
    }
}